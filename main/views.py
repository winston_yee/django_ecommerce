from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone

from django.shortcuts import render_to_response
from django.template import RequestContext


def index(request):
    return render_to_response(
        'index.html', context_instance=RequestContext(request)
    )
